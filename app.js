// const serverless = require('serverless-http');
const express = require('express');
const bodyParser = require('body-parser');

// Crear servidor
const app = express();

// Configuración de middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/', require('./routes/index'));

app.listen(3000, () => {
  console.log('server on: localhost:3000');
});

// module.exports.handler = serverless(app);

// NIVEL 1 PRUEBA TÉCNICA

// Hay 4 instrucciones diferentes:
// 1 - hacia la derecha
// 2 - diagonal derecha
// 3 - abajo
// 4 - diagonal izquierda

// La función isValid(...) revisa dos cosas:
// 1. Si nos puede mover hacia cierta coordenada según el número de instrucción.
// 2. Si hay al menos tres renglones abajo (para el movimiento en diagonal o
//    hacia abajo, no tiene sentido que revisemos las demás bases si hay menos de 3, pues
//    podemos asumir que ahí ya no se juntarán 4 bases iguales)
function isValid(row, column, N, instruction, originalRow) {
  switch (instruction) {
    case 1:
      return column + 1 < N;
    case 2:
      return (row + 1 < N && column + 1 < N) && originalRow + 3 < N;
    case 3:
      return row + 1 < N && originalRow + 3 < N;
    case 4:
      return (row + 1 < N && column - 1 >= 0) && originalRow + 3 < N;
    default:
      break;
  }
}

// La función increment(...) permite incrementar el contador de las bases nitrogenadas
// y las secuencias en caso de que se junten 4 bases consecutivas.
function increment(currentBase, A, T, G, C) {
  switch (currentBase) {
    case 'A':
      A++;
      break;
    case 'T':
      T++;
      break;
    case 'G':
      G++;
      break;
    case 'C':
      C++;
      break;
    default:
      break;
  }
  return [A, T, G, C];
}

// La función iterate(...) nos permite movernos a través del arrego bidimensional.

// La bandera endFlag checa si ya terminamos de iterar a través de la matriz.
// Sólo se activa en caso de que no hayan mutaciones y se haya terminado de iterar.

// El arreglo pathArray guardará las coordenadas de las bases nitrogenadas
// por las que nos vayamos moviendo cuando detectemos que pudiera existir una secuencia.
// En caso de que se junte una secuencia de 4 bases iguales, todas las coordenadas
// que guardamos en pathArray las vamos a sustituir por un '*' para no
// ir a contar una misma base en dos secuencias diferentes.
function iterate(
  matrix, sequences, countA, countT, countG, countC,
  row, column, instruction, N, originalCoordinates, endFlag, pathArray,
) {
  if (sequences === 2) {
    return true;
  }
  if (endFlag) return false;

  // Según el número de instrucción se revisará la "celda" de la derecha (1),
  // abajo (3) o diagonal (2 y 4).
  switch (instruction) {
    case 1:
      if (isValid(row, column, N, 1) && matrix[row][column] === matrix[row][column + 1]) {
        const currentBase = matrix[row][column];
        pathArray.push([row, column]);
        [countA, countT, countG, countC] = increment(
          currentBase, countA, countT, countG, countC,
        );
        if (countA === 4 || countT === 4 || countC === 4 || countG === 4) {
          sequences++;
          pathArray.push([row, column + 1]);
          pathArray.forEach((coord) => {
            matrix[coord[0]][coord[1]] = '*';
          });
          pathArray = [];
        }
        return iterate(
          matrix, sequences, countA, countT, countG, countC,
          row, column + 1, 1, N, originalCoordinates, endFlag, pathArray,
        );
      }
      return iterate(
        matrix, sequences, 1, 1, 1, 1,
        originalCoordinates[0], originalCoordinates[1], 2, N, originalCoordinates, endFlag, [],
      );

    case 2:
      if (
        isValid(row, column, N, 2, originalCoordinates[0])
       && matrix[row][column] === matrix[row + 1][column + 1]
      ) {
        const currentBase = matrix[row][column];
        pathArray.push([row, column]);
        [countA, countT, countG, countC] = increment(
          currentBase, countA, countT, countG, countC,
        );
        if (countA === 4 || countT === 4 || countC === 4 || countG === 4) {
          sequences++;
          pathArray.push([row + 1, column + 1]);
          pathArray.forEach((coord) => {
            matrix[coord[0]][coord[1]] = '*';
          });
          pathArray = [];
        }
        return iterate(
          matrix, sequences, countA, countT, countG, countC,
          row + 1, column + 1, 2, N, originalCoordinates, endFlag, pathArray,
        );
      }
      return iterate(
        matrix, sequences, 1, 1, 1, 1,
        originalCoordinates[0], originalCoordinates[1], 3, N, originalCoordinates, endFlag, [],
      );
    case 3:
      if (
        isValid(row, column, N, 3, originalCoordinates[0])
       && matrix[row][column] === matrix[row + 1][column]
      ) {
        const currentBase = matrix[row][column];
        pathArray.push([row, column]);
        [countA, countT, countG, countC] = increment(
          currentBase, countA, countT, countG, countC,
        );
        if (countA === 4 || countT === 4 || countC === 4 || countG === 4) {
          sequences++;
          pathArray.push([row + 1, column]);
          pathArray.forEach((coord) => {
            matrix[coord[0]][coord[1]] = '*';
          });
          pathArray = [];
        }
        return iterate(
          matrix, sequences, countA, countT, countG, countC,
          row + 1, column, 3, N, originalCoordinates, endFlag, pathArray,
        );
      }
      return iterate(
        matrix, sequences, 1, 1, 1, 1,
        originalCoordinates[0], originalCoordinates[1], 4, N, originalCoordinates, endFlag, [],
      );
    case 4:
      if (
        isValid(row, column, N, 4, originalCoordinates[0])
       && matrix[row][column] === matrix[row + 1][column - 1]
      ) {
        const currentBase = matrix[row][column];
        pathArray.push([row, column]);
        [countA, countT, countG, countC] = increment(
          currentBase, countA, countT, countG, countC,
        );
        if (countA === 4 || countT === 4 || countC === 4 || countG === 4) {
          sequences++;
          pathArray.push([row + 1, column - 1]);
          pathArray.forEach((coord) => {
            matrix[coord[0]][coord[1]] = '*';
          });
          pathArray = [];
        }
        return iterate(
          matrix, sequences, countA, countT, countG, countC,
          row + 1, column - 1, 4, N, originalCoordinates, endFlag, pathArray,
        );
      }
      if (originalCoordinates[0] + 1 === N && originalCoordinates[1] + 1 === N) {
        endFlag = true;
        // Sólo volvemos a llamar la función para que la endFlag nos saque de la recursión.
        return iterate(
          matrix, sequences, 1, 1, 1, 1,
          originalCoordinates[0], originalCoordinates[1], 1, N, originalCoordinates, endFlag, [],
        );
      }
      if (originalCoordinates[1] + 1 === N) {
        originalCoordinates = [originalCoordinates[0] + 1, 0];
      } else originalCoordinates[1]++;
      return iterate(
        matrix, sequences, 1, 1, 1, 1,
        originalCoordinates[0], originalCoordinates[1], 1, N, originalCoordinates, endFlag, [],
      );
    default:
      break;
  }
}

module.exports = function hasMutation(dna) {
  // Validar que tenga al menos 4 elementos.
  if (dna.length < 4) return false;
  // Validar que cada uno de los elementos tenga la misma longitud que el arreglo
  // y que sólo contenga las letras ATGC
  const filteredDNA = dna.filter((el) => (el.length === dna.length && /^[ATGC]+$/.test(el.toUpperCase())));
  if (filteredDNA.length !== dna.length) return false;
  const N = dna.length;
  const matrix = dna.map((chain) => chain.split(''));
  // Pasamos los valores iniciales o "default" a la función iterate(...)
  return iterate(
    matrix, 0, 1, 1, 1, 1, 0, 0, 1, N, [0, 0], false, [],
  );
};

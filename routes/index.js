const router = require('express').Router();
const { checkForMutation, getStats } = require('../controllers/mutation-controllers');

router.post('/mutation', checkForMutation);
router.get('/stats', getStats);

module.exports = router;

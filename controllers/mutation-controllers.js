/* eslint-disable no-unused-expressions */
require('dotenv').config();
const hasMutation = require('../checkDNAMutation');
const pg = require('../db/dbConnect');

exports.checkForMutation = (req, res) => {
  const { dna } = req.body;
  if (hasMutation(dna)) {
    pg.query('INSERT INTO verified_dnas VALUES ($1, $2)', [1, dna.toString()], (error) => {
      if (error) {
        throw error;
      }
      // res.status(200).send('DNA with mutation saved to DB');
    });
    res.sendStatus(200);
  } else {
    pg.query('INSERT INTO verified_dnas VALUES ($1, $2)', [0, dna.toString()], (error) => {
      if (error) {
        throw error;
      }
      // res.status(403).send('DNA without mutation saved to DB');
    });
    res.sendStatus(403);
  }
};

exports.getStats = (req, res) => {
  let allDnas;
  let mutation = 0;
  let noMutation = 0;
  pg.query('SELECT * FROM verified_dnas', (error, response) => {
    if (error) {
      throw error;
    }
    allDnas = response.rows;
    allDnas && allDnas.forEach((dna) => {
      dna.hasMutation === '1' ? mutation++ : noMutation++;
    });
    res.status(200).json({
      count_mutations: mutation,
      count_no_mutation: noMutation,
      ratio: mutation / noMutation,
    });
  });
};

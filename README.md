## DNA Mutation Project 🧬

Para correr el proyecto localmente:
```sh
$ git clone git@gitlab.com:mariana-gamino/dna-mutation-project.git
$ cd dna-mutation-project
$ cp .env.example .env
```
- Dentro del archivo .env copiar las siguientes credenciales:
```sh
RDS_USERNAME=mariana
RDS_HOSTNAME=database-dna.cxjtxsnp0dws.us-east-1.rds.amazonaws.com
RDS_DATABASE=databasedna
RDS_PASSWORD=pwddnadb
RDS_PORT=5432
```
- Ejecutar el siguiente comando:
```sh
$ npm i && npm start
```

- Utilizar alguna herramenta como Postman para probar el endpoint:
      `POST /mutation`

Sample body para `POST /mutation`
```sh
{
 "dna":["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"]
}
```

- Utilizar el navegador para probar el endpoint:
      `GET /stats`
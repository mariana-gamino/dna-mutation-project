module.exports = {
  env: {
    browser: true,
    es2020: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 11,
    sourceType: 'module',
  },
  rules: {
  },
  overrides: [{
    files: ['*'],
    rules: {
      // 'function-call-argument-newline': ['error', 'never'],
      'function-call-argument-newline': 0,
      'max-len': ['error', { code: 115 }],
      'consistent-return': 0,
      'no-param-reassign': 0,
      'no-plusplus': 0,
    },
  }],
};

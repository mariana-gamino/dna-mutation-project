const { Pool } = require('pg'); // node-postgres module (for interfacing with our PostgreSQL db)
const dbConfig = require('./config');

const pool = new Pool(dbConfig);

module.exports = pool;
